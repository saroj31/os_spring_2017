#include <stdio.h>
#include <errno.h>
#include <sys/syscall.h>
#include <sys/signal.h>

#define the_goob(arg) syscall(325,arg)
#define init_cnt(pid) syscall(326,pid)
#define get_cnt(signumber) syscall(327,signumber)

void happy()
{
  int ret,stop;
  printf("child is happy\n");
  ret = get_cnt(SIGCONT);
  stop = get_cnt(SIGCONT);
  printf("SIGCONT has been sent to child %d times\n",ret);
  printf("SIGCONT has been sent to child %d times\n",stop);

}

int main()
{
	int pid,ret,i,j;
	i=5; j=5;
	//ret = the_goob(5);
	switch(pid=fork()){
		case 0:
			while(1){

			signal(SIGCONT,happy);
			printf("child is playing\n");
			sleep(1);
			}
			break;
		default:
			init_cnt(pid);	
			while(1)
			{
			printf("parent is going to sleep\n");
			sleep(1);
			printf("parent.....wakes up .... checks on child \n");
			ret = kill(pid,SIGCONT);
			//ret = kill(pid,SIGUSR1);
			sleep(2);
			printf("kill returned %d\n",ret);
			ret = kill(pid,SIGCONT);
			//ret = kill(pid,SIGUSR2);
			
			}		
	
	}
	fprintf(stderr,"syscall returned %d\n");
	return 0;
}


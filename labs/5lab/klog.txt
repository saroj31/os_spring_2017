
[    0.000000] Initializing cgroup subsys cpuset
[    0.000000] Initializing cgroup subsys cpu
[    0.000000] Initializing cgroup subsys cpuacct
[    0.000000] Linux version 4.3.3 (root@vm0) (gcc version 4.8.5 20150623 (Red Hat 4.8.5-4) (GCC) ) #2 SMP Tue Feb 28 16:50:25 EST 2017
[    0.000000] Command line: BOOT_IMAGE=/vmlinuz-4.3.3 root=/dev/mapper/centos-root ro rd.lvm.lv=centos/root rd.lvm.lv=centos/swap LANG=en_US.UTF-8
[    0.000000] x86/fpu: Legacy x87 FPU detected.
[    0.000000] x86/fpu: Using 'lazy' FPU context switches.
[    0.000000] e820: BIOS-provided physical RAM map:
[    0.000000] BIOS-e820: [mem 0x0000000000000000-0x000000000009fbff] usable
[    0.000000] BIOS-e820: [mem 0x000000000009fc00-0x000000000009ffff] reserved
[    0.000000] BIOS-e820: [mem 0x00000000000f0000-0x00000000000fffff] reserved
[    0.000000] BIOS-e820: [mem 0x0000000000100000-0x00000000bfffdfff] usable
[    0.000000] BIOS-e820: [mem 0x00000000bfffe000-0x00000000bfffffff] reserved
[    0.000000] BIOS-e820: [mem 0x00000000feffc000-0x00000000feffffff] reserved
[    0.000000] BIOS-e820: [mem 0x00000000fffc0000-0x00000000ffffffff] reserved
[    0.000000] BIOS-e820: [mem 0x0000000100000000-0x000000013fffffff] usable
[    0.000000] NX (Execute Disable) protection: active
[    0.000000] SMBIOS 2.4 present.
[    0.000000] DMI: Red Hat KVM, BIOS 0.5.1 01/01/2011
[    0.000000] Hypervisor detected: KVM
[    0.000000] e820: update [mem 0x00000000-0x00000fff] usable ==> reserved
[    0.000000] e820: remove [mem 0x000a0000-0x000fffff] usable
[    0.000000] e820: last_pfn = 0x140000 max_arch_pfn = 0x400000000
[    0.000000] MTRR default type: write-back
[    0.000000] MTRR fixed ranges enabled:
[    0.000000]   00000-9FFFF write-back
[    0.000000]   A0000-BFFFF uncachable
[    0.000000]   C0000-FFFFF write-protect
[    0.000000] MTRR variable ranges enabled:
[    0.000000]   0 base 0000C0000000 mask 3FFFC0000000 uncachable
[    0.000000]   1 disabled
[    0.000000]   2 disabled
[    0.000000]   3 disabled
[    0.000000]   4 disabled
[    0.000000]   5 disabled
[    0.000000]   6 disabled
[    0.000000]   7 disabled
[    0.000000] x86/PAT: PAT not supported by CPU.
[    0.000000] e820: last_pfn = 0xbfffe max_arch_pfn = 0x400000000
[    0.000000] found SMP MP-table at [mem 0x000f2000-0x000f200f] mapped at [ffff8800000f2000]
[    0.000000] Base memory trampoline at [ffff880000099000] 99000 size 24576
[    0.000000] init_memory_mapping: [mem 0x00000000-0x000fffff]
[    0.000000]  [mem 0x00000000-0x000fffff] page 4k
[    0.000000] BRK [0x01ff7000, 0x01ff7fff] PGTABLE
[    0.000000] BRK [0x01ff8000, 0x01ff8fff] PGTABLE
[    0.000000] BRK [0x01ff9000, 0x01ff9fff] PGTABLE
[    0.000000] init_memory_mapping: [mem 0x13fe00000-0x13fffffff]
[    0.000000]  [mem 0x13fe00000-0x13fffffff] page 2M
[    0.000000] BRK [0x01ffa000, 0x01ffafff] PGTABLE
[    0.000000] init_memory_mapping: [mem 0x120000000-0x13fdfffff]
[    0.000000]  [mem 0x120000000-0x13fdfffff] page 2M
[    0.000000] init_memory_mapping: [mem 0x00100000-0xbfffdfff]
[    0.000000]  [mem 0x00100000-0x001fffff] page 4k
[    0.000000]  [mem 0x00200000-0xbfdfffff] page 2M
[    0.000000]  [mem 0xbfe00000-0xbfffdfff] page 4k
[    0.000000] init_memory_mapping: [mem 0x100000000-0x11fffffff]
[    0.000000]  [mem 0x100000000-0x11fffffff] page 2M
[    0.000000] RAMDISK: [mem 0x32b4f000-0x3559ffff]
[    0.000000] ACPI: Early table checksum verification disabled
[    0.000000] ACPI: RSDP 0x00000000000F1E40 000014 (v00 BOCHS )
[    0.000000] ACPI: RSDT 0x00000000BFFFFA9B 000034 (v01 BOCHS  BXPCRSDT 00000001 BXPC 00000001)
[    0.000000] ACPI: FACP 0x00000000BFFFF177 000074 (v01 BOCHS  BXPCFACP 00000001 BXPC 00000001)
[    0.000000] ACPI: DSDT 0x00000000BFFFE040 001137 (v01 BXPC   BXDSDT   00000001 INTL 20130823)
[    0.000000] ACPI: FACS 0x00000000BFFFE000 000040
[    0.000000] ACPI: SSDT 0x00000000BFFFF1EB 000838 (v01 BOCHS  BXPCSSDT 00000001 BXPC 00000001)
[    0.000000] ACPI: APIC 0x00000000BFFFFA23 000078 (v01 BOCHS  BXPCAPIC 00000001 BXPC 00000001)
[    0.000000] ACPI: RSDT 0x00000000BFFFFA9B 000034 (v01 BOCHS  BXPCRSDT 00000001 BXPC 00000001)
[    0.000000] ACPI: Local APIC address 0xfee00000
[    0.000000] No NUMA configuration found
[    0.000000] Faking a node at [mem 0x0000000000000000-0x000000013fffffff]
[    0.000000] NODE_DATA(0) allocated [mem 0x13ffd6000-0x13fffcfff]
[    0.000000] kvm-clock: Using msrs 4b564d01 and 4b564d00
[    0.000000] kvm-clock: cpu 0, msr 1:3ff56001, primary cpu clock
[    0.000000] clocksource: kvm-clock: mask: 0xffffffffffffffff max_cycles: 0x1cd42e4dffb, max_idle_ns: 881590591483 ns
[    0.000000]  [ffffea0000000000-ffffea0004ffffff] PMD -> [ffff88013b600000-ffff88013f5fffff] on node 0
[    0.000000] Zone ranges:
[    0.000000]   DMA      [mem 0x0000000000001000-0x0000000000ffffff]
[    0.000000]   DMA32    [mem 0x0000000001000000-0x00000000ffffffff]
[    0.000000]   Normal   [mem 0x0000000100000000-0x000000013fffffff]
[    0.000000] Movable zone start for each node
[    0.000000] Early memory node ranges
[    0.000000]   node   0: [mem 0x0000000000001000-0x000000000009efff]
[    0.000000]   node   0: [mem 0x0000000000100000-0x00000000bfffdfff]
[    0.000000]   node   0: [mem 0x0000000100000000-0x000000013fffffff]
[    0.000000] Initmem setup node 0 [mem 0x0000000000001000-0x000000013fffffff]
[    0.000000] On node 0 totalpages: 1048476
[    0.000000]   DMA zone: 64 pages used for memmap
[    0.000000]   DMA zone: 21 pages reserved
[    0.000000]   DMA zone: 3998 pages, LIFO batch:0
[    0.000000]   DMA32 zone: 12224 pages used for memmap
[    0.000000]   DMA32 zone: 782334 pages, LIFO batch:31
[    0.000000]   Normal zone: 4096 pages used for memmap
[    0.000000]   Normal zone: 262144 pages, LIFO batch:31
[    0.000000] ACPI: PM-Timer IO Port: 0x608
[    0.000000] ACPI: Local APIC address 0xfee00000
[    0.000000] ACPI: LAPIC_NMI (acpi_id[0xff] dfl dfl lint[0x1])
[    0.000000] IOAPIC[0]: apic_id 0, version 17, address 0xfec00000, GSI 0-23
[    0.000000] ACPI: INT_SRC_OVR (bus 0 bus_irq 0 global_irq 2 dfl dfl)
[    0.000000] ACPI: INT_SRC_OVR (bus 0 bus_irq 5 global_irq 5 high level)
[    0.000000] ACPI: INT_SRC_OVR (bus 0 bus_irq 9 global_irq 9 high level)
[    0.000000] ACPI: INT_SRC_OVR (bus 0 bus_irq 10 global_irq 10 high level)
[    0.000000] ACPI: INT_SRC_OVR (bus 0 bus_irq 11 global_irq 11 high level)
[    0.000000] ACPI: IRQ0 used by override.
[    0.000000] ACPI: IRQ5 used by override.
[    0.000000] ACPI: IRQ9 used by override.
[    0.000000] ACPI: IRQ10 used by override.
[    0.000000] ACPI: IRQ11 used by override.
[    0.000000] Using ACPI (MADT) for SMP configuration information
[    0.000000] smpboot: Allowing 1 CPUs, 0 hotplug CPUs
[    0.000000] PM: Registered nosave memory: [mem 0x00000000-0x00000fff]
[    0.000000] PM: Registered nosave memory: [mem 0x0009f000-0x0009ffff]
[    0.000000] PM: Registered nosave memory: [mem 0x000a0000-0x000effff]
[    0.000000] PM: Registered nosave memory: [mem 0x000f0000-0x000fffff]
[    0.000000] PM: Registered nosave memory: [mem 0xbfffe000-0xbfffffff]
[    0.000000] PM: Registered nosave memory: [mem 0xc0000000-0xfeffbfff]
[    0.000000] PM: Registered nosave memory: [mem 0xfeffc000-0xfeffffff]
[    0.000000] PM: Registered nosave memory: [mem 0xff000000-0xfffbffff]
[    0.000000] PM: Registered nosave memory: [mem 0xfffc0000-0xffffffff]
[    0.000000] e820: [mem 0xc0000000-0xfeffbfff] available for PCI devices
[    0.000000] Booting paravirtualized kernel on KVM
[    0.000000] clocksource: refined-jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 1910969940391419 ns
[    0.000000] setup_percpu: NR_CPUS:8192 nr_cpumask_bits:1 nr_cpu_ids:1 nr_node_ids:1
[    0.000000] PERCPU: Embedded 34 pages/cpu @ffff88013fc00000 s98392 r8192 d32680 u2097152
[    0.000000] pcpu-alloc: s98392 r8192 d32680 u2097152 alloc=1*2097152
[    0.000000] pcpu-alloc: [0] 0 
[    0.000000] KVM setup async PF for cpu 0
[    0.000000] kvm-stealtime: cpu 0, msr 13fc0d980
[    0.000000] PV qspinlock hash table entries: 256 (order: 0, 4096 bytes)
[    0.000000] Built 1 zonelists in Node order, mobility grouping on.  Total pages: 1032071
[    0.000000] Policy zone: Normal
[    0.000000] Kernel command line: BOOT_IMAGE=/vmlinuz-4.3.3 root=/dev/mapper/centos-root ro rd.lvm.lv=centos/root rd.lvm.lv=centos/swap LANG=en_US.UTF-8
[    0.000000] PID hash table entries: 4096 (order: 3, 32768 bytes)
[    0.000000] Memory: 4001460K/4193904K available (6704K kernel code, 1373K rwdata, 3256K rodata, 1752K init, 3212K bss, 192444K reserved, 0K cma-reserved)
[    0.000000] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=1, Nodes=1
[    0.000000] Hierarchical RCU implementation.
[    0.000000] 	Build-time adjustment of leaf fanout to 64.
[    0.000000] 	RCU restricting CPUs from NR_CPUS=8192 to nr_cpu_ids=1.
[    0.000000] RCU: Adjusting geometry for rcu_fanout_leaf=64, nr_cpu_ids=1
[    0.000000] NR_IRQS:524544 nr_irqs:256 16
[    0.000000] 	Offload RCU callbacks from all CPUs
[    0.000000] 	Offload RCU callbacks from CPUs: 0.
[    0.000000] Console: colour VGA+ 80x25
[    0.000000] console [tty0] enabled
[    0.000000] tsc: Detected 2699.998 MHz processor
[    0.005000] Calibrating delay loop (skipped) preset value.. 5399.99 BogoMIPS (lpj=2699998)
[    0.006008] pid_max: default: 32768 minimum: 301
[    0.007015] ACPI: Core revision 20150818
[    0.010794] ACPI: 2 ACPI AML tables successfully acquired and loaded
[    0.012034] Security Framework initialized
[    0.013007] SELinux:  Initializing.
[    0.014020] SELinux:  Starting in permissive mode
[    0.015314] Dentry cache hash table entries: 524288 (order: 10, 4194304 bytes)
[    0.018749] Inode-cache hash table entries: 262144 (order: 9, 2097152 bytes)
[    0.020552] Mount-cache hash table entries: 8192 (order: 4, 65536 bytes)
[    0.021019] Mountpoint-cache hash table entries: 8192 (order: 4, 65536 bytes)
[    0.023292] Initializing cgroup subsys io
[    0.024026] Initializing cgroup subsys memory
[    0.025020] Initializing cgroup subsys devices
[    0.026021] Initializing cgroup subsys freezer
[    0.027009] Initializing cgroup subsys net_cls
[    0.028009] Initializing cgroup subsys perf_event
[    0.029008] Initializing cgroup subsys hugetlb
[    0.030122] mce: CPU supports 10 MCE banks
[    0.032019] Last level iTLB entries: 4KB 0, 2MB 0, 4MB 0
[    0.034007] Last level dTLB entries: 4KB 0, 2MB 0, 4MB 0, 1GB 0
[    0.057825] Freeing SMP alternatives memory: 24K (ffffffff81ccc000 - ffffffff81cd2000)
[    0.072015] ftrace: allocating 26332 entries in 103 pages
[    0.086821] ..TIMER: vector=0x30 apic1=0 pin1=2 apic2=-1 pin2=-1
[    0.087000] smpboot: CPU0: Intel QEMU Virtual CPU version 1.5.3 (family: 0x6, model: 0xd, stepping: 0x3)
[    0.088043] Performance Events: Broken PMU hardware detected, using software events only.
[    0.090004] Failed to access perfctr msr (MSR c2 is 0)
[    0.091016] KVM setup paravirtual spinlock
[    0.093117] x86: Booted up 1 node, 1 CPUs
[    0.094005] smpboot: Total of 1 processors activated (5399.99 BogoMIPS)
[    0.095421] devtmpfs: initialized
[    0.100286] evm: security.selinux
[    0.101003] evm: security.ima
[    0.102002] evm: security.capability
[    0.103104] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 1911260446275000 ns
[    0.104104] atomic64_test: passed for x86-64 platform with CX8 and with SSE
[    0.105006] pinctrl core: initialized pinctrl subsystem
[    0.107087] NET: Registered protocol family 16
[    0.108216] cpuidle: using governor menu
[    0.110057] ACPI: bus type PCI registered
[    0.111004] acpiphp: ACPI Hot Plug PCI Controller Driver version: 0.5
[    0.112165] PCI: Using configuration type 1 for base access
[    0.114972] ACPI: Added _OSI(Module Device)
[    0.115004] ACPI: Added _OSI(Processor Device)
[    0.116003] ACPI: Added _OSI(3.0 _SCP Extensions)
[    0.117003] ACPI: Added _OSI(Processor Aggregator Device)
[    0.120766] ACPI: Interpreter enabled
[    0.121007] ACPI Exception: AE_NOT_FOUND, While evaluating Sleep State [\_S1_] (20150818/hwxface-580)
[    0.123004] ACPI Exception: AE_NOT_FOUND, While evaluating Sleep State [\_S2_] (20150818/hwxface-580)
[    0.124715] ACPI Exception: AE_NOT_FOUND, While evaluating Sleep State [\_S3_] (20150818/hwxface-580)
[    0.126004] ACPI Exception: AE_NOT_FOUND, While evaluating Sleep State [\_S4_] (20150818/hwxface-580)
[    0.128007] ACPI: (supports S0 S5)
[    0.128751] ACPI: Using IOAPIC for interrupt routing
[    0.129018] PCI: Using host bridge windows from ACPI; if necessary, use "pci=nocrs" and report a bug
[    0.132655] ACPI: PCI Root Bridge [PCI0] (domain 0000 [bus 00-ff])
[    0.133007] acpi PNP0A03:00: _OSC: OS supports [ASPM ClockPM Segments MSI]
[    0.134007] acpi PNP0A03:00: _OSC failed (AE_NOT_FOUND); disabling ASPM
[    0.135050] acpi PNP0A03:00: fail to add MMCONFIG information, can't access extended PCI configuration space under this bridge.
[    0.137123] acpiphp: Slot [3] registered
[    0.138047] acpiphp: Slot [4] registered
[    0.139054] acpiphp: Slot [5] registered
[    0.140050] acpiphp: Slot [6] registered
[    0.141043] acpiphp: Slot [7] registered
[    0.142043] acpiphp: Slot [8] registered
[    0.143054] acpiphp: Slot [9] registered
[    0.145017] acpiphp: Slot [10] registered
[    0.146050] acpiphp: Slot [11] registered
[    0.147040] acpiphp: Slot [12] registered
[    0.148040] acpiphp: Slot [13] registered
[    0.149040] acpiphp: Slot [14] registered
[    0.150039] acpiphp: Slot [15] registered
[    0.151043] acpiphp: Slot [16] registered
[    0.152052] acpiphp: Slot [17] registered
[    0.153040] acpiphp: Slot [18] registered
[    0.155004] acpiphp: Slot [19] registered
[    0.156037] acpiphp: Slot [20] registered
[    0.157037] acpiphp: Slot [21] registered
[    0.158037] acpiphp: Slot [22] registered
[    0.159040] acpiphp: Slot [23] registered
[    0.160043] acpiphp: Slot [24] registered
[    0.161038] acpiphp: Slot [25] registered
[    0.162038] acpiphp: Slot [26] registered
[    0.163035] acpiphp: Slot [27] registered
[    0.164035] acpiphp: Slot [28] registered
[    0.165038] acpiphp: Slot [29] registered
[    0.166044] acpiphp: Slot [30] registered
[    0.167039] acpiphp: Slot [31] registered
[    0.168023] PCI host bridge to bus 0000:00
[    0.169013] pci_bus 0000:00: root bus resource [bus 00-ff]
[    0.170005] pci_bus 0000:00: root bus resource [io  0x0000-0x0cf7 window]
[    0.171004] pci_bus 0000:00: root bus resource [io  0x0d00-0xffff window]
[    0.172004] pci_bus 0000:00: root bus resource [mem 0x000a0000-0x000bffff window]
[    0.173003] pci_bus 0000:00: root bus resource [mem 0xc0000000-0xfebfffff window]
[    0.174047] pci 0000:00:00.0: [8086:1237] type 00 class 0x060000
[    0.175016] pci 0000:00:01.0: [8086:7000] type 00 class 0x060100
[    0.175616] pci 0000:00:01.1: [8086:7010] type 00 class 0x010180
[    0.181613] pci 0000:00:01.1: reg 0x20: [io  0xc060-0xc06f]
[    0.183606] pci 0000:00:01.1: legacy IDE quirk: reg 0x10: [io  0x01f0-0x01f7]
[    0.184004] pci 0000:00:01.1: legacy IDE quirk: reg 0x14: [io  0x03f6]
[    0.185010] pci 0000:00:01.1: legacy IDE quirk: reg 0x18: [io  0x0170-0x0177]
[    0.186003] pci 0000:00:01.1: legacy IDE quirk: reg 0x1c: [io  0x0376]
[    0.187160] pci 0000:00:01.3: [8086:7113] type 00 class 0x068000
[    0.187608] pci 0000:00:01.3: quirk: [io  0x0600-0x063f] claimed by PIIX4 ACPI
[    0.188014] pci 0000:00:01.3: quirk: [io  0x0700-0x070f] claimed by PIIX4 SMB
[    0.189205] pci 0000:00:02.0: [1234:1113] type 00 class 0x030000
[    0.191005] pci 0000:00:02.0: reg 0x10: [mem 0xfe000000-0xfe0fffff pref]
[    0.193004] pci 0000:00:02.0: reg 0x14: [mem 0xfe100000-0xfe10ffff pref]
[    0.195004] pci 0000:00:02.0: reg 0x18: [mem 0xfc000000-0xfdffffff pref]
[    0.204015] pci 0000:00:02.0: reg 0x30: [mem 0xfebd0000-0xfebdffff pref]
[    0.204452] pci 0000:00:03.0: [1af4:1000] type 00 class 0x020000
[    0.206007] pci 0000:00:03.0: reg 0x10: [io  0xc040-0xc05f]
[    0.208006] pci 0000:00:03.0: reg 0x14: [mem 0xfebf0000-0xfebf0fff]
[    0.222006] pci 0000:00:03.0: reg 0x30: [mem 0xfebe0000-0xfebeffff pref]
[    0.222349] pci 0000:00:04.0: [1af4:1004] type 00 class 0x010000
[    0.224006] pci 0000:00:04.0: reg 0x10: [io  0xc000-0xc03f]
[    0.226005] pci 0000:00:04.0: reg 0x14: [mem 0xfebf1000-0xfebf1fff]
[    0.238799] ACPI: PCI Interrupt Link [LNKA] (IRQs 5 *10 11)
[    0.240130] ACPI: PCI Interrupt Link [LNKB] (IRQs 5 *10 11)
[    0.241127] ACPI: PCI Interrupt Link [LNKC] (IRQs 5 10 *11)
[    0.243119] ACPI: PCI Interrupt Link [LNKD] (IRQs 5 10 *11)
[    0.244067] ACPI: PCI Interrupt Link [LNKS] (IRQs *9)
[    0.245815] acpi LNXCPU:00: Invalid PBLK length [0]
[    0.246297] ACPI: Enabled 16 GPEs in block 00 to 0F
[    0.248131] vgaarb: setting as boot device: PCI:0000:00:02.0
[    0.248980] vgaarb: device added: PCI:0000:00:02.0,decodes=io+mem,owns=io+mem,locks=none
[    0.249003] vgaarb: loaded
[    0.250002] vgaarb: bridge control possible 0000:00:02.0
[    0.251079] SCSI subsystem initialized
[    0.252020] ACPI: bus type USB registered
[    0.252838] usbcore: registered new interface driver usbfs
[    0.253022] usbcore: registered new interface driver hub
[    0.254024] usbcore: registered new device driver usb
[    0.255134] PCI: Using ACPI for IRQ routing
[    0.256010] PCI: pci_cache_line_size set to 64 bytes
[    0.256137] e820: reserve RAM buffer [mem 0x0009fc00-0x0009ffff]
[    0.256139] e820: reserve RAM buffer [mem 0xbfffe000-0xbfffffff]
[    0.256275] NetLabel: Initializing
[    0.257003] NetLabel:  domain hash size = 128
[    0.258002] NetLabel:  protocols = UNLABELED CIPSOv4
[    0.258804] NetLabel:  unlabeled traffic allowed by default
[    0.259060] clocksource: Switched to clocksource kvm-clock
[    0.266827] pnp: PnP ACPI init
[    0.305095] pnp 00:00: Plug and Play ACPI device, IDs PNP0b00 (active)
[    0.305151] pnp 00:01: Plug and Play ACPI device, IDs PNP0303 (active)
[    0.305194] pnp 00:02: Plug and Play ACPI device, IDs PNP0f13 (active)
[    0.305235] pnp 00:03: [dma 2]
[    0.305262] pnp 00:03: Plug and Play ACPI device, IDs PNP0700 (active)
[    0.305390] pnp 00:04: Plug and Play ACPI device, IDs PNP0501 (active)
[    0.305733] pnp: PnP ACPI: found 5 devices
[    0.313881] clocksource: acpi_pm: mask: 0xffffff max_cycles: 0xffffff, max_idle_ns: 2085701024 ns
[    0.316051] pci_bus 0000:00: resource 4 [io  0x0000-0x0cf7 window]
[    0.316055] pci_bus 0000:00: resource 5 [io  0x0d00-0xffff window]
[    0.316058] pci_bus 0000:00: resource 6 [mem 0x000a0000-0x000bffff window]
[    0.316061] pci_bus 0000:00: resource 7 [mem 0xc0000000-0xfebfffff window]
[    0.316114] NET: Registered protocol family 2
[    0.317466] TCP established hash table entries: 32768 (order: 6, 262144 bytes)
[    0.319645] TCP bind hash table entries: 32768 (order: 7, 524288 bytes)
[    0.320955] TCP: Hash tables configured (established 32768 bind 32768)
[    0.322192] UDP hash table entries: 2048 (order: 4, 65536 bytes)
[    0.323404] UDP-Lite hash table entries: 2048 (order: 4, 65536 bytes)
[    0.324646] NET: Registered protocol family 1
[    0.325719] pci 0000:00:00.0: Limiting direct PCI/PCI transfers
[    0.326819] pci 0000:00:01.0: PIIX3: Enabling Passive Release
[    0.327923] pci 0000:00:01.0: Activating ISA DMA hang workarounds
[    0.329072] pci 0000:00:02.0: Video device with shadowed ROM
[    0.329112] PCI: CLS 0 bytes, default 64
[    0.329197] Unpacking initramfs...
[    0.973414] Freeing initrd memory: 43332K (ffff880032b4f000 - ffff8800355a0000)
[    0.974627] PCI-DMA: Using software bounce buffering for IO (SWIOTLB)
[    0.975277] software IO TLB [mem 0xbbffe000-0xbfffe000] (64MB) mapped at [ffff8800bbffe000-ffff8800bfffdfff]
[    1.008376] microcode: CPU0 sig=0x6d3, pf=0x1, revision=0x1
[    1.009090] microcode: Microcode Update Driver: v2.00 <tigran@aivazian.fsnet.co.uk>, Peter Oruba
[    1.010459] sha1_ssse3: Neither AVX nor AVX2 nor SSSE3 is available/usable.
[    1.011152] sha256_ssse3: Neither AVX nor SSSE3 is available/usable.
[    1.011972] futex hash table entries: 256 (order: 2, 16384 bytes)
[    1.012661] audit: initializing netlink subsys (disabled)
[    1.013330] audit: type=2000 audit(1488479289.588:1): initialized
[    1.014186] Initialise system trusted keyring
[    1.014897] HugeTLB registered 2 MB page size, pre-allocated 0 pages
[    1.017027] zbud: loaded
[    1.017810] VFS: Disk quotas dquot_6.6.0
[    1.018436] VFS: Dquot-cache hash table entries: 512 (order 0, 4096 bytes)
[    1.019231] Key type big_key registered
[    1.019824] SELinux:  Registering netfilter hooks
[    1.023589] NET: Registered protocol family 38
[    1.024232] Key type asymmetric registered
[    1.024821] Asymmetric key parser 'x509' registered
[    1.025461] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 251)
[    1.026613] io scheduler noop registered
[    1.027238] io scheduler deadline registered (default)
[    1.027885] io scheduler cfq registered
[    1.028564] pci_hotplug: PCI Hot Plug PCI Core version: 0.5
[    1.029204] pciehp: PCI Express Hot Plug Controller Driver version: 0.4
[    1.029873] intel_idle: does not run on family 6 model 13
[    1.029929] input: Power Button as /devices/LNXSYSTM:00/LNXPWRBN:00/input/input0
[    1.031076] ACPI: Power Button [PWRF]
[    1.031832] GHES: HEST is not enabled!
[    1.032496] Serial: 8250/16550 driver, 4 ports, IRQ sharing enabled
[    1.064809] 00:04: ttyS0 at I/O 0x3f8 (irq = 4, base_baud = 115200) is a 16550A
[    1.066262] Non-volatile memory driver v1.3
[    1.066880] Linux agpgart interface v0.103
[    1.067635] rdac: device handler registered
[    1.068262] hp_sw: device handler registered
[    1.068862] emc: device handler registered
[    1.069460] alua: device handler registered
[    1.070093] libphy: Fixed MDIO Bus: probed
[    1.070744] ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
[    1.071387] ehci-pci: EHCI PCI platform driver
[    1.072023] ohci_hcd: USB 1.1 'Open' Host Controller (OHCI) Driver
[    1.072646] ohci-pci: OHCI PCI platform driver
[    1.073259] uhci_hcd: USB Universal Host Controller Interface driver
[    1.073933] usbcore: registered new interface driver usbserial
[    1.074580] usbcore: registered new interface driver usbserial_generic
[    1.075240] usbserial: USB Serial support registered for generic
[    1.075890] i8042: PNP: PS/2 Controller [PNP0303:KBD,PNP0f13:MOU] at 0x60,0x64 irq 1,12
[    1.077639] serio: i8042 KBD port at 0x60,0x64 irq 1
[    1.078258] serio: i8042 AUX port at 0x60,0x64 irq 12
[    1.078954] mousedev: PS/2 mouse device common for all mice
[    1.079868] input: AT Translated Set 2 keyboard as /devices/platform/i8042/serio0/input/input1
[    1.082580] rtc_cmos 00:00: RTC can wake from S4
[    1.083430] rtc_cmos 00:00: rtc core: registered rtc_cmos as rtc0
[    1.084157] rtc_cmos 00:00: alarms up to one day, 114 bytes nvram
[    1.084879] hidraw: raw HID events driver (C) Jiri Kosina
[    1.085617] usbcore: registered new interface driver usbhid
[    1.086250] usbhid: USB HID core driver
[    1.086898] drop_monitor: Initializing network drop monitor service
[    1.087626] Initializing XFRM netlink socket
[    1.088321] NET: Registered protocol family 10
[    1.089083] NET: Registered protocol family 17
[    1.089887] registered taskstats version 1
[    1.090508] Loading compiled-in X.509 certificates
[    1.091951] Loaded X.509 cert 'Build time autogenerated kernel key: fe8b30be80ba59f61113fac2dc28f4f7e2d1d2d2'
[    1.093141] zswap: loaded using pool lzo/zbud
[    1.123480] Key type trusted registered
[    1.125389] Key type encrypted registered
[    1.126021] ima: No TPM chip found, activating TPM-bypass!
[    1.126668] evm: HMAC attrs: 0x1
[    1.127543] rtc_cmos 00:00: setting system clock to 2017-03-02 18:28:09 UTC (1488479289)
[    1.129234] Freeing unused kernel memory: 1752K (ffffffff81b16000 - ffffffff81ccc000)
[    1.131989] random: systemd urandom read with 4 bits of entropy available
[    1.133360] systemd[1]: systemd 219 running in system mode. (+PAM +AUDIT +SELINUX +IMA -APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ -LZ4 -SECCOMP +BLKID +ELFUTILS +KMOD +IDN)
[    1.135199] systemd[1]: Detected virtualization kvm.
[    1.135824] systemd[1]: Detected architecture x86-64.
[    1.136455] systemd[1]: Running in initial RAM disk.
[    1.139108] systemd[1]: Set hostname to <vm0>.
[    1.182537] systemd[1]: Created slice -.slice.
[    1.183178] systemd[1]: Starting -.slice.
[    1.184830] systemd[1]: Listening on udev Control Socket.
[    1.185477] systemd[1]: Starting udev Control Socket.
[    1.187136] systemd[1]: Reached target Local File Systems.
[    1.187797] systemd[1]: Starting Local File Systems.
[    1.189508] systemd[1]: Created slice System Slice.
[    1.190157] systemd[1]: Starting System Slice.
[    1.191771] systemd[1]: Reached target Timers.
[    1.192412] systemd[1]: Starting Timers.
[    1.194104] systemd[1]: Listening on Journal Socket.
[    1.194725] systemd[1]: Starting Journal Socket.
[    1.195673] systemd[1]: Starting Journal Service...
[    1.197488] systemd[1]: Starting Setup Virtual Console...
[    1.209035] systemd[1]: Reached target Slices.
[    1.209047] systemd[1]: Starting Slices.
[    1.209085] systemd[1]: Started dracut ask for additional cmdline parameters.
[    1.209468] systemd[1]: Starting dracut cmdline hook...
[    1.682129] systemd[1]: Started Load Kernel Modules.
[    1.683865] systemd[1]: Starting Create list of required static device nodes for the current kernel...
[    1.697877] systemd[1]: Listening on udev Kernel Socket.
[    1.698855] systemd[1]: Starting udev Kernel Socket.
[    1.703983] systemd[1]: Reached target Sockets.
[    1.776031] systemd[1]: Starting Sockets.
[    1.809855] systemd[1]: Reached target Swap.
[    1.810565] systemd[1]: Starting Swap.
[    1.811672] systemd[1]: Starting Apply Kernel Variables...
[    1.814905] systemd[1]: Started Journal Service.
[    1.901417] device-mapper: uevent: version 1.0.3
[    1.910349] device-mapper: ioctl: 4.33.0-ioctl (2015-8-18) initialised: dm-devel@redhat.com
[    1.971815] input: ImExPS/2 Generic Explorer Mouse as /devices/platform/i8042/serio1/input/input3
[    2.028833] tsc: Refined TSC clocksource calibration: 2700.059 MHz
[    2.029891] clocksource: tsc: mask: 0xffffffffffffffff max_cycles: 0x26eb7613e68, max_idle_ns: 440795242339 ns
[    2.234087] Floppy drive(s): fd0 is 1.44M
[    2.236568] ACPI: PCI Interrupt Link [LNKC] enabled at IRQ 11
[    2.238487] virtio-pci 0000:00:03.0: virtio_pci: leaving for legacy driver
[    2.241179] ACPI: PCI Interrupt Link [LNKD] enabled at IRQ 10
[    2.241875] virtio-pci 0000:00:04.0: virtio_pci: leaving for legacy driver
[    2.245390] FDC 0 is a S82078B
[    2.303638] scsi host0: Virtio SCSI HBA
[    2.314103] libata version 3.00 loaded.
[    2.314198] scsi 0:0:0:0: Direct-Access     QEMU     QEMU HARDDISK    1.5. PQ: 0 ANSI: 5
[    2.357133] ata_piix 0000:00:01.1: version 2.13
[    2.364082] scsi host1: ata_piix
[    2.369061] scsi host2: ata_piix
[    2.369109] ata1: PATA max MWDMA2 cmd 0x1f0 ctl 0x3f6 bmdma 0xc060 irq 14
[    2.369111] ata2: PATA max MWDMA2 cmd 0x170 ctl 0x376 bmdma 0xc068 irq 15
[    2.522386] ata2.01: NODEV after polling detection
[    2.522617] ata2.00: ATAPI: QEMU DVD-ROM, 1.5.3, max UDMA/100
[    2.523713] ata2.00: configured for MWDMA2
[    2.524774] scsi 2:0:0:0: CD-ROM            QEMU     QEMU DVD-ROM     1.5. PQ: 0 ANSI: 5
[    2.570734] sd 0:0:0:0: [sda] 62914560 512-byte logical blocks: (32.2 GB/30.0 GiB)
[    2.572680] sd 0:0:0:0: [sda] Write Protect is off
[    2.573308] sd 0:0:0:0: [sda] Mode Sense: 63 00 00 08
[    2.573562] sd 0:0:0:0: [sda] Write cache: enabled, read cache: enabled, doesn't support DPO or FUA
[    2.576476]  sda: sda1 sda2
[    2.577667] sd 0:0:0:0: [sda] Attached SCSI disk
[    2.580899] sr 2:0:0:0: [sr0] scsi3-mmc drive: 4x/4x cd/rw xa/form2 tray
[    2.581613] cdrom: Uniform CD-ROM driver Revision: 3.20
[    2.582696] sr 2:0:0:0: Attached scsi CD-ROM sr0
[    3.204153] SGI XFS with ACLs, security attributes, no debug enabled
[    3.207421] XFS (dm-0): Mounting V4 Filesystem
[    3.333835] XFS (dm-0): Ending clean mount
[    3.847691] systemd-journald[108]: Received SIGTERM from PID 1 (systemd).
[    3.908808] audit: type=1404 audit(1488479292.280:2): enforcing=1 old_enforcing=0 auid=4294967295 ses=4294967295
[    3.939856] random: nonblocking pool is initialized
[    3.953377] SELinux: 32768 avtab hash slots, 112391 rules.
[    3.985990] SELinux: 32768 avtab hash slots, 112391 rules.
[    4.026126] SELinux:  8 users, 105 roles, 4954 types, 302 bools, 1 sens, 1024 cats
[    4.026130] SELinux:  83 classes, 112391 rules
[    4.032067] SELinux:  Class netlink_iscsi_socket not defined in policy.
[    4.032929] SELinux:  Class netlink_fib_lookup_socket not defined in policy.
[    4.033785] SELinux:  Class netlink_connector_socket not defined in policy.
[    4.068722] SELinux:  Class netlink_netfilter_socket not defined in policy.
[    4.069545] SELinux:  Class netlink_generic_socket not defined in policy.
[    4.070346] SELinux:  Class netlink_scsitransport_socket not defined in policy.
[    4.071743] SELinux:  Class netlink_rdma_socket not defined in policy.
[    4.072548] SELinux:  Class netlink_crypto_socket not defined in policy.
[    4.073355] SELinux:  Permission audit_read in class capability2 not defined in policy.
[    4.074740] SELinux:  Class binder not defined in policy.
[    4.075516] SELinux: the above unknown classes and permissions will be allowed
[    4.076921] SELinux:  Completing initialization.
[    4.076922] SELinux:  Setting up existing superblocks.
[    4.081318] audit: type=1403 audit(1488479292.453:3): policy loaded auid=4294967295 ses=4294967295
[    4.087799] systemd[1]: Successfully loaded SELinux policy in 179.871ms.
[    4.154607] ip_tables: (C) 2000-2006 Netfilter Core Team
[    4.155512] systemd[1]: Inserted module 'ip_tables'
[    4.183195] systemd[1]: Relabelled /dev and /run in 23.680ms.
[    5.016031] systemd-journald[750]: Received request to flush runtime journal from PID 1
[    5.150382] RPC: Registered named UNIX socket transport module.
[    5.150383] RPC: Registered udp transport module.
[    5.150384] RPC: Registered tcp transport module.
[    5.150385] RPC: Registered tcp NFSv4.1 backchannel transport module.
[    5.618527] Installing knfsd (copyright (C) 1996 okir@monad.swb.de).
[    6.020646] input: PC Speaker as /devices/platform/pcspkr/input/input4
[    6.088400] ACPI: No IRQ available for PCI Interrupt Link [LNKS]. Try pci=noacpi or acpi=off
[    6.089572] piix4_smbus 0000:00:01.3: PCI INT A: no GSI
[    6.110485] sd 0:0:0:0: Attached scsi generic sg0 type 0
[    6.137650] piix4_smbus 0000:00:01.3: SMBus Host Controller at 0x700, revision 0
[    6.211642] Adding 3145724k swap on /dev/mapper/centos-swap.  Priority:-1 extents:1 across:3145724k FS
[    6.261269] sr 2:0:0:0: Attached scsi generic sg1 type 5
[    6.263046] ppdev: user-space parallel port driver
[    6.491302] XFS (sda1): Mounting V4 Filesystem
[    6.651687] XFS (sda1): Ending clean mount
[    6.914940] audit: type=1305 audit(1488479295.286:4): audit_pid=951 old=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:auditd_t:s0 res=1
[    8.019352] nf_conntrack version 0.5.0 (16384 buckets, 65536 max)
[    8.134944] ip6_tables: (C) 2000-2006 Netfilter Core Team
[    8.273716] Ebtables v2.0 registered
[    8.448896] bridge: automatic filtering via arp/ip/ip6tables has been deprecated. Update your scripts to load br_netfilter if you need this.
[    8.990864] IPv6: ADDRCONF(NETDEV_UP): eth0: link is not ready
[   10.374910] IPv6: eth0: IPv6 duplicate address fe80::5054:ff:fe12:3456 detected!
[   15.647279] tun: Universal TUN/TAP device driver, 1.6
[   15.647804] tun: (C) 1999-2004 Max Krasnyansky <maxk@qualcomm.com>
[   15.654465] device virbr0-nic entered promiscuous mode
[   16.144770] virbr0: port 1(virbr0-nic) entered listening state
[   16.144776] virbr0: port 1(virbr0-nic) entered listening state
[   16.219589] virbr0: port 1(virbr0-nic) entered disabled state
[   20.174054] Adjusting kvm-clock more than 11% (9437003 vs 9311354)
[  252.668694] mymod: module license 'Proprietary' taints kernel.
[  252.668907] Disabling lock debugging due to kernel taint
[  252.669116] mymod: module verification failed: signature and/or required key missing - tainting kernel
[  252.670811] Kyuoko3 probe is being called
[  252.670968] init function from kernel
[  252.774826] Kyouko_Kernel open
[  252.775218] bind_dma: addr after vm_mmap ffffffffffffffea 
[  252.775425] KYOUKO3:ioctl:bind_dma arg passed accessok
[  252.775585] KYOUKO3::ioctl: copy_to_user did not work. ret = 8
[  252.776039] a.out[2808]: segfault at 0 ip 0000000000400a93 sp 00007ffc4157f800 error 6 in a.out[400000+1000]
[  252.880963] kyouko3_release: BUUH BYE
[  349.994086] kyouko3_remove Call recieved
[  349.995401] Exit Function Kernel
[  351.006060] Kyuoko3 probe is being called
[  351.006544] init function from kernel
[  351.116590] Kyouko_Kernel open
[  351.117214] bind_dma: addr after vm_mmap ffffffffffffffea 
[  351.117813] KYOUKO3:ioctl:bind_dma arg passed accessok
[  351.117960] KYOUKO3::ioctl: copy_to_user did not work. ret = 8
[  351.119641] a.out[3141]: segfault at 0 ip 0000000000400a93 sp 00007ffca3bb63a0 error 6 in a.out[400000+1000]
[  353.919137] kyouko3_release: BUUH BYE

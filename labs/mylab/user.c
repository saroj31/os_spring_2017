#include "mymod.h"

#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <stdint.h>
#include <sys/mman.h>
#include <linux/types.h>
#include <linux/ioctl.h>
#include <linux/types.h>

#define WIDTH 1024
#define HEIGHT 768
#define PIXEL_SIZE 4

struct kyouko3_dma_hdr{
	uint32_t address:14;
	uint32_t count:10;
	uint32_t opcode:8;
};

struct u_kyuoko_device {

  unsigned int *u_control_base;
  unsigned int *u_frame_base;
  unsigned int *u_buff_base;
  int fDesc;

}kyouko3;

unsigned int U_READ_REG(unsigned int rgister){

	return (*(kyouko3.u_control_base+(rgister>>2)));
}

void U_WRITE_FB(int i,unsigned int color){

	*(kyouko3.u_frame_base + i) = color;
}

//Enter FIFO entry of command and value into the FIFO in Kernel
void Queue_FIFO(unsigned int uiCommand, unsigned int uiValue){

  struct fifo_entry fifo_entry = {uiCommand,uiValue};
  ioctl(kyouko3.fDesc,FIFO_QUEUE,&fifo_entry);

  return;
}

int drawTriangles(){

   int ret = 0;
   unsigned int *u_base, *u_curr;
   unsigned long byte_count;
   unsigned long u_buff_base;

   struct kyouko3_dma_hdr dma_header = { //32bits or 4 bytes
      .address = 0x1045,
      .count = 0,
      .opcode = 0x14
  };

  //srand(time(NULL));
  ret = ioctl(kyouko3.fDesc,BIND_DMA, &u_buff_base);
  printf("dmabuffer0 address:0x%lx\n",u_buff_base);
  //*u_base  = *(unsigned int *)&dma_header;
  //memcpy((float *)u_buff_base,&dma_header,sizeof(dma_header));
  ret = ioctl(kyouko3.fDesc,UBIND_DMA, 0);
  return ret;
}

int main(void)
{
	int fd,i;
	unsigned int result;
	unsigned int frameSize = WIDTH * HEIGHT * PIXEL_SIZE;
	unsigned long u_buff_base;


	fd = open("/dev/kyouko3",O_RDWR);
	kyouko3.fDesc = fd;

	ioctl(fd,VMODE,GRAPHICS_ON);
	ioctl(fd, FIFO_FLUSH, 0);
	//**/

	//Drawing the triangle at a specific position
	drawTriangles();

	sleep(2);

	ioctl(fd,VMODE,GRAPHICS_OFF);
	close(fd);
	return 0;
}

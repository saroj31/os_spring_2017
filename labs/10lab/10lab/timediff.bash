#!/bin/bash

START=$(date +%s)
END=$(date +%s)
DIFF=$(( $END - $START ))

while [ $DIFF -le 2 ]
do
echo "$DIFF"
END=$(date +%s)
DIFF=$(( $END - $START ))
done
